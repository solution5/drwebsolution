#include <string>
#include <map>
#include <iostream>

template<class TKey, class TValue>
class Dictionary
{
public:
    virtual ~Dictionary() = default;

    virtual const TValue& Get(const TKey& key) const = 0;
    virtual void Set(const TKey& key, const TValue& value) = 0;
    virtual bool isSet(const TKey& key) const = 0;
};

template<class TKey>
class NotFoundException : public std::exception
{
public:
    virtual const TKey& GetKey() const noexcept = 0;
};

template<class TKey>
class NFExc : public NotFoundException<TKey> 
{
public:
    virtual const TKey& GetKey() const noexcept
    {
        return Key;
    }

    NFExc(const TKey& key) : Key(key) { }

private:
    TKey Key;
};

template<class TKey, class TValue>
class Dict : public Dictionary<TKey, TValue> 
{
public:
    virtual const TValue& Get(const TKey& key) const
    {
        auto it = Map.find(key);
        if (it == Map.end())
            throw NFExc<TKey>(key);
        return it->second;
    }

    virtual void Set(const TKey& key, const TValue& value)
    {
        Map[key] = value;
    }

    virtual bool isSet(const TKey& key) const 
    {
        return Map.find(key) != Map.end();
    }

private:
    std::map<TKey, TValue> Map;
};

int main()
{
    Dict<std::string, int> Dict;
    Dict.Set("hello", 1);
    Dict.Set("hai", 11);
    Dict.Set("world", 21);
    Dict.Set("world", 12);
    try 
    {
        std::cout << Dict.Get("world") << '\n';
        std::cout << Dict.Get("hai") << '\n';
    }
    catch (const NotFoundException<std::string> &Exeception)
    {
        std::cout << "Error: key " << Exeception.GetKey() << " not found." << '\n';
    }
}
